<%@page import="cl.inacap.Ev2CW.libros.dto.Libro"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Odiseo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

</head>

<style>

    .marginT{
        margin-top: 40px;
    }

    .col-centered{
        float: none;
        margin: 0 auto;
    }

    .btn:hover{
        background-color:rgb(245, 119, 1);
    }
</style>
<body>


<jsp:include page="Header.jsp"/>

    <section>
					<%  
					
					Libro l = new Libro(); 
					
					l = (Libro) request.getAttribute("Libro");
					
					
					
					
					%>
        <div class="container" style="margin-top:20px;">

            <div class="row">


                <div class="col-md-12 text-center">

                    <img src="<%=l.getPortadaLibro()%>" class="img-fluid" alt="Responsive image" width="200" height="200">
		
                </div>
					
					
				
					
                <div class="col-md-12 text-center" style="margin-top:20px;">
               		<h3><%=l.getAutorLibro() %> </h1>
                </div>
                <div class="col-md-12 text-center">
               		<h3><%=l.getNombreLibro() %></h3>
               </div>
                <div class="col-md-12 text-center">
                	<h3><%=l.getCategoriaLibro() %></h3>
                </div>

            </div>

            <div class="row" style="margin-top:20px;">

                <div class="col-md-12 text-center">
                    <h3>Descripcion</h3>
                </div>

                <div class="col-md-12">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur pariatur exercitationem nihil adipisci illum vero impedit, quasi repellendus voluptatibus cupiditate molestias accusamus ab iure debitis repellat enim odio eos reprehenderit.
                </div>
            </div>

        </div>



    </section>


</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>