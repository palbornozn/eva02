<%@page import="cl.inacap.Ev2CW.libros.dto.Libro"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Odiseo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

</head>
<style>

    .marginT{
        margin-top: 40px;
    }

    .col-centered{
        float: none;
        margin: 0 auto;
    }

    .btn:hover{
        background-color:rgb(245, 119, 1);
    }
</style>
<body>
    
<jsp:include page="Header.jsp"/>



<section>


        <div class="container">

                <div class="row">


                        <div class="col-md-12 text-center" style="margin-top: 40px;">
                                <h4>Listado Libros</h4>

                                <table class="table table-dark">
                                        <thead>
                                                <tr>
                                                        <!--  <th>#</th>-->
                                                        <th>Libro</th>
                                                        <th>Autor</th>
                                                        <th>Estrellas</th>
                                                        <th>Detalle</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                        	<%  
                                        	
                                        	List<Libro> l =(List<Libro>) request.getAttribute("ListadoLibro");
                                        	
                                        	for(int i =0; i<l.size();i++){
                                        		
                                        	
                                        	%>
                                                <tr>
                                                        <!-- <td><%/*=l.get(i).getIdLibro() */%></td> -->
                                                        <td><%=l.get(i).getNombreLibro() %></td>
                                                        <td><%=l.get(i).getAutorLibro() %></td>
                                                        <td><%
                                                        
                                                       
                                                        int estrellas = l.get(i).getEstrellas();
                                                        
                                                        for(int k=0;k<estrellas;k++){
                                                        
                                                        	%>
                                                        	
                                                        <img src="https://images.vexels.com/media/users/3/134121/isolated/lists/5ff73adb05d7f1fe47dd49bb1b08affa-icono-de-dibujos-animados-estrella-50.png" width="10" height="10" alt="" />
                                                        <%} %>
                                                        </td>
                                                        <td> <a  href="Detalle.do?index=<%=l.get(i).getIdLibro() %>">Detalle</a></td>
												</tr>
                                                
                                             <%} %>   
                                        </tbody>

                                </table>


                        </div>

                </div>

        </div>



    
</section>






</body>




<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


</html>