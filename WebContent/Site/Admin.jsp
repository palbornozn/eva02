<%@page import="cl.inacap.Ev2CW.libros.dao.LibroDAO"%>
<%@page import="cl.inacap.Ev2CW.libros.dto.Libro"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Odiseo</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<style>

    .marginT{
        margin-top: 40px;
    }

    .col-centered{
        float: none;
        margin: 0 auto;
    }

    .btn:hover{
        background-color:rgb(245, 119, 1);
    }
    
    .Wh{
    
    margin-left:20px; 
    margin-right:20px; 
    margin-top:20px;
    font-style: italic;
    
    }
    
         .col-centered{
        float: none;
        margin: 0 auto;
    }
</style>
<body>

<jsp:include page="Header.jsp"/>



<section>

 <div class="row marginT">

                <div class="col-md-12 text-center">

                    <h1>
                        Agregar Libros
                    </h1>

                </div>

            </div>

	<div class="container">
	
		<form class="col-centered text-center border" action="CRUD.do" method="post">
			
			
			<%List<Libro> l =(List<Libro>) request.getAttribute("ListadoLibro"); %>
			
			<div class="col-md-12 col-centered">
				<label for="nombre"> Nombre </label>
				<input name="nombre"  type="text" />
			</div>
			
			<div class="col-md-12 col-centered">
				<label for="autor"> Autor </label>
				<input name="autor" id="autor" type="text" />
			</div>
			
			<div class="col-md-12 col-centered">
				<label for="cantPaginas">  paginas </label>
				<input  name ="cantPaginas" id="cantPaginas" type="text" />
			</div>
			
			<div class="col-md-3 col-centered">
			<label for="destacado">Destacado</label>
					<select name="destacado" class="custom-select">
						  <option value="TRUE">Si</option>
						  <option value="FALSE">No</option>
					
					</select>
			</div>
			
			<div class="col-md-12">
			<label for="Portada"> Portada </label>
			<input name="portada" id="Portada" type="text" />
			</div>
			
			<div class="col-md-12">
					
				<label for="estrellas"> Estrellas </label>
				<input name="estrellas" id="estrellas" type="text" />
			</div>
			
			
			<div class="col-md-12">
	
				<label for="categoria"> ID Categoria </label>
				<input name="IdCategoria" id="categoria" type="text" />
		
			</div>
			
		
			<div class="col-md-3 col-centered">
					<select name=categoria class="custom-select">
						  <option value="Fantasia">Fantasia</option>
						  <option value="Novela">Novela</option>
						  <option value="Clasico">Clasico</option>
					
					</select>
			</div>
			
		
		
				 <input class="btn btn-primary" type="submit" />
				<!--<button type="button" onclick="CRUD.do" class="btn btn-primary">Agregar</button>-->
			
				
		</form>
	
	
	</div>

</section>




<section>










</section>




<section>


            <div class="row marginT">

                <div class="col-md-12 text-center">

                    <h1>
                        Panel de edicion
                    </h1>

                </div>

            </div>
	
			
	
		
        <div class="container">

                <div class="row">


                        <div class="col-md-12 text-center" style="margin-top: 40px;">
                                <h4>Listado Libros</h4>

                                <table class="table table-dark">
                                        <thead>
                                                <tr>
                                                        <th>#</th>
                                                        <th>Libro</th>
                                                        <th>Autor</th>
                                                        <th>Cantidad de paginas</th>
                                                        <th>Categoria</th>
                                                        <th>Destacado</th>
                                                        <th>Estrellas</th> 
                                                 
                                             
                                                        <th>Eliminar</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                        	<%  
                                        	
                                        	int guardado;
                                        	
                                        	
                                        	for(int i =0; i<l.size();i++){
                                        		
                                        	
                                        	%>
                                                <tr>
                                                        <td><%=l.get(i).getIdLibro() %></td>
                                                        <td><%=l.get(i).getNombreLibro() %></td>
                                                        <td><%=l.get(i).getAutorLibro() %></td>
                                                        <td><%=l.get(i).getCantPaginas() %></td>
                                                        <td><%=l.get(i).getCategoriaLibro() %></td>
                                                        <td><%=l.get(i).isBoleanDestacado() %></td>
                                                        <td><%=l.get(i).getEstrellas() %></td>
                                         
                                 
                                  						<td> <a class="btn btn-danger" href="Eliminar.do?index=<%=l.get(i).getIdLibro() %>"> Eliminar </a> </td>
                                  				
												</tr>
                                                
                                             <%} %>   
                                        </tbody>

                                </table>


                        </div>

                </div>

        </div>

	


           
    
</section> 

						
						
						
						<div class="col-md-12 text-center col-centered">
							
							<h3>Editar</h3>
							
						</div>
						
						
							<div class="col-md-3 col-centered">
							
							
								<select name=categoria id=editar onchange="editar()" class="custom-select">
								<%
								int activo = 0;
								for(int i=0;i<l.size();i++){
								
								%>
									  <option  value="<%=l.get(i).getIdLibro()%>"><%=l.get(i).getNombreLibro() %></option>
									
								<%  
								
								
								
								}
								
								%>
								</select>
						</div>
						
						
					
						
				
						
					
<section>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id=myModal>Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
       
       <form class="col-centered text-center border" action="Editar.do" method="post">
       
       	<div class="col-md-12 col-centered">
				<label for="nombre"> ID </label>
				<input name="indice" placeholder="<%=l.get(activo).getNombreLibro() %>" type="text" />
			</div>
			
			<div class="col-md-12 col-centered">
				<label for="nombre"> Nombre </label>
				<input name="nombre" placeholder="<%=l.get(activo).getNombreLibro() %>" type="text" />
			</div>
			
			<div class="col-md-12 col-centered">
				<label for="autor"> Autor </label>
				<input name="autor" placeholder="<%=l.get(activo).getAutorLibro() %>"id="autor" type="text" />
			</div>
			
			<div class="col-md-12 col-centered">
				<label for="cantPaginas">  paginas </label>
				<input  name ="cantPaginas" placeholder="<%=l.get(activo).getCantPaginas() %>"id="cantPaginas" type="text" />
			</div>
			
			<div class="col-md-3 col-centered">
					<select name="destacado" class="custom-select">
						  <option value="TRUE">Si</option>
						  <option value="FALSE">No</option>
					
					</select>
			</div>
			
			<div class="col-md-12">
			<label for="Portada"> Portada </label>
			<input name="portada" placeholder="<%=l.get(activo).getPortadaLibro() %>" id="Portada" type="text" />
			</div>
			
			<div class="col-md-12">
					
				<label for="estrellas"> Estrellas </label>
				<input name="estrellas" placeholder="<%=l.get(activo).getEstrellas() %>" id="estrellas" type="text" />
			</div>
			
			
			<div class="col-md-12">
	
				<label for="categoria"> ID Categoria </label>
				<input name="IdCategoria" placeholder="<%=l.get(activo).getIdCategoria() %>" id="categoria" type="text" />
		
			</div>
			
		
			<div class="col-md-3 col-centered">
					<select name=categoria class="custom-select">
						  <option value="Fantasia">Fantasia</option>
						  <option value="Novela">Novela</option>
						  <option value="Clasico">Clasico</option>
					
					</select>
			</div>
			
		
		
				 <input class="btn btn-primary" type="submit" />
       
       </form>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
       <!--   <button type="button" class="btn btn-primary">Ejecutar</button>-->
      </div>
    </div>
  </div>
</div>



</section>




</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


<script>

function editar(){
		
	
	var valor = document.getElementById('editar').value;
		  $("#exampleModal").modal("show");
		  
}

</script>


</html>