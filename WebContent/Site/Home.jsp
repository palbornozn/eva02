<%@page import="cl.inacap.Ev2CW.libros.dto.CategoriaLibro"%>
<%@page import="cl.inacap.Ev2CW.libros.dto.Libro"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Odiseo</title>
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<style>

    .marginT{
        margin-top: 40px;
    }

    .col-centered{
        float: none;
        margin: 0 auto;
    }

    .btn:hover{
        background-color:rgb(245, 119, 1);
    }
    
    .Wh{
    
    margin-left:20px; 
    margin-right:20px; 
    margin-top:20px;
    font-style: italic;
    
    }
</style>


<body >
    

<jsp:include page="Header.jsp"/>

	<%
	
	List<Libro> l = (List<Libro>) request.getAttribute("ListadoLibro");
	List<CategoriaLibro> c = (List<CategoriaLibro>) request.getAttribute("ListadoCategorias");

	
	%>

<section>

    <div class="row marginT" >
	
	
        <div class="col-md-8 bordered" style="background-color:rgb(77, 96, 97)">
            <h3>Destacados Fantasia</h3>
        </div>
        <div class="col-md-4">
        </div>
   
        <%
        
        for(int i=0;i<l.size();i++){
        	if(l.get(i).getIdCategoria()==1 && l.get(i).getEstrellas()>4){
        %>
        <div class="row">
        <div class="col-md-5 text-center Wh" >
        	
        	
        	<img src="<%=l.get(i).getPortadaLibro() %>" width="200" height="200" />
        	<hr />
        	<a class="btn"href="Detalle.do?index=<%=l.get(i).getIdLibro() %>" ><%=l.get(i).getNombreLibro() %></a>
        	<h8 ><%=l.get(i).getAutorLibro() %></h8>
        	<br />
        	<h8 > <% 
                                                       
                                                        int estrellas = l.get(i).getEstrellas();
                                                        
                                                        for(int k=0;k<estrellas;k++){
                                                        
                                                        	%>
                                                        	
                                                        <img src="https://images.vexels.com/media/users/3/134121/isolated/lists/5ff73adb05d7f1fe47dd49bb1b08affa-icono-de-dibujos-animados-estrella-50.png" width="10" height="10" alt="" />
                                                        <%} %></h8>
        	
        	
        </div>
        </div>
        <%  
        	}
        }
        %>
    </div>

	

</section>



<section>

    <div class="row marginT" >

  <div class="col-md-8 bordered" style="background-color:rgb(77, 96, 97)">
            <h3>Destacados Novelas</h3>
        </div>
        <div class="col-md-4">
        </div>
   
        <%
        
        for(int i=0;i<l.size();i++){
        	if(l.get(i).getIdCategoria()==2 && l.get(i).getEstrellas()>4){
        %>
        <div class="row">
        <div class="col-md-5 text-center Wh" >
        	
        	
        	<img src="<%=l.get(i).getPortadaLibro() %>" width="200" height="200" />
        	<hr />
        	<a class="btn"href="Detalle.do?index=<%=l.get(i).getIdLibro() %>" ><%=l.get(i).getNombreLibro() %></a>
        	<h8 ><%=l.get(i).getAutorLibro() %></h8>
        	<br />
        <h8 > <% 
                                                       
                                                        int estrellas = l.get(i).getEstrellas();
                                                        
                                                        for(int k=0;k<estrellas;k++){
                                                        
                                                        	%>
                                                        	
                                                        <img src="https://images.vexels.com/media/users/3/134121/isolated/lists/5ff73adb05d7f1fe47dd49bb1b08affa-icono-de-dibujos-animados-estrella-50.png" width="10" height="10" alt="" />
                                                        <%} %></h8>
        	
        </div>
        </div>
        <%  
        	}
        }
        %>
        
        
        
    </div>

    

</section>

<section>

    <div class="row marginT" >

       <div class="col-md-8 bordered" style="background-color:rgb(77, 96, 97)">
            <h3>Destacados Clasicos</h3>
        </div>
        <div class="col-md-4">
        </div>
   
        <%
        
        for(int i=0;i<l.size();i++){
        	if(l.get(i).getIdCategoria()==3 && l.get(i).getEstrellas()>4){
        %>
        <div class="row">
        <div class="col-md-5 text-center Wh" >
        	
        	
        	<img src="<%=l.get(i).getPortadaLibro() %>" width="200" height="200" />
        	<hr />
        	<a class="btn"href="Detalle.do?index=<%=l.get(i).getIdLibro() %>" ><%=l.get(i).getNombreLibro() %></a>
        	<h8 ><%=l.get(i).getAutorLibro() %></h8>
        	<br />
     <h8 > <% 
                                                       
                                                        int estrellas = l.get(i).getEstrellas();
                                                        
                                                        for(int k=0;k<estrellas;k++){
                                                        
                                                        	%>
                                                        	
                                                        <img src="https://images.vexels.com/media/users/3/134121/isolated/lists/5ff73adb05d7f1fe47dd49bb1b08affa-icono-de-dibujos-animados-estrella-50.png" width="10" height="10" alt="" />
                                                        <%} %></h8>
        </div>
        <%  
        	}
        }
        %>
        
        
        
    </div>

    

</section>


</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>