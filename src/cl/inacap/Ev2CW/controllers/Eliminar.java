package cl.inacap.Ev2CW.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class Eliminar
 */
@WebServlet("/Eliminar.do")
public class Eliminar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Eliminar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TRAER EL INDICE

		int index = Integer.parseInt(request.getParameter("index"));
		System.out.println(index);
		LibroDAO listaLibro = new LibroDAO();
		Libro l = new Libro();
		l = listaLibro.getLibroById(index);

		listaLibro = LectorCsv.eliminarCSV(index);
		
		request.getRequestDispatcher("Admin.do").forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	
		/*
		 	int id = l.getIdLibro(); 
			String nombre = l.getNombreLibro();
			String autor = l.getAutorLibro();
			int cantParginas = l.getCantPaginas();
			String categoria= l.getCategoriaLibro();
			boolean destacado = l.isBoleanDestacado();
			int estrellas = l.getEstrellas();
			String portada = l.getPortadaLibro();
			booean 
			*/
	
		 
	}

}
