package cl.inacap.Ev2CW.controllers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.inacap.Ev2CW.libros.dao.CategoriaDAO;
import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.CategoriaLibro;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home.do")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
			// CARGAR LOS LIBROS
			LibroDAO listaLibro = new LibroDAO();
			listaLibro.deleteAll();
			listaLibro = LectorCsv.readCSV();
			List<Libro> l= listaLibro.getAll();
			
			// CARGAR LAS CATEGORIAS
		
				
				
		request.setAttribute("ListadoLibro", l);			
		request.setAttribute("titulo", "Home");
		request.getRequestDispatcher("Site/Home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
