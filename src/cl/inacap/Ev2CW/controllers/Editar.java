package cl.inacap.Ev2CW.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class Editar
 */
@WebServlet("/Editar.do")
public class Editar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Editar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LibroDAO listaLibros= new LibroDAO();
		listaLibros.deleteAll();
		listaLibros = LectorCsv.readCSV();
		//List<Libro> l= listaLibros.getAll();
		
		Libro libro = new Libro();
		
		int indice = Integer.parseInt(request.getParameter("indice"));
		

		
		libro = listaLibros.getLibroById(indice);
		
		libro.setNombreLibro(request.getParameter("nombre"));
		libro.setAutorLibro(request.getParameter("autor"));
		libro.setCantPaginas(Integer.parseInt(request.getParameter("cantPaginas")));
		libro.setCategoriaLibro(request.getParameter("categoria"));
		libro.setBoleanDestacado(Boolean.parseBoolean(request.getParameter("destacado")));
		libro.setEstrellas(Integer.parseInt(request.getParameter("estrellas")));
		libro.setPortadaLibro(request.getParameter("portada"));
		libro.setIdCategoria(Integer.parseInt(request.getParameter("IdCategoria")));
		
		LectorCsv.editarCSV(libro);
	
		
		
		response.sendRedirect("Admin.do");
	}

}
