package cl.inacap.Ev2CW.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class ListarController
 */
@WebServlet("/ListarController.do")
public class ListarController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		LibroDAO listaLibros= new LibroDAO();
		listaLibros.deleteAll();
		listaLibros = LectorCsv.readCSV();
		List<Libro> l= listaLibros.getAll();
		
		//LibroDAO listaLibros= new LibroDAO();
		//List<Libro> l= listaLibros.getAll();
		request.setAttribute("ListadoLibro", l);
		request.getRequestDispatcher("Site/ListadoLibro.jsp").forward(request, response);
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
