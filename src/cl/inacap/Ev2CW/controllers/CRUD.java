package cl.inacap.Ev2CW.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class CRUD
 */
@WebServlet("/CRUD.do")
public class CRUD extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CRUD() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
		
		//LectorCsv.escribirCSV(Cadena,true);
 
 
 		/*
		Libro l = new Libro();
		
		String nombre = request.getParameter("nombre");
		String autor = request.getParameter("autor");
		int paginas = Integer.parseInt(request.getParameter("cantPaginas"));
		int categoria= Integer.parseInt(request.getParameter("categoria"));
		boolean destacado = Boolean.parseBoolean(request.getParameter("destacado"));
		int estrellas = Integer.parseInt(request.getParameter("estrellas"));
		String portada = request.getParameter("portada");
		
		
		l.setAutorLibro(nombre);
		l.setAutorLibro(autor);
		l.setCantPaginas(paginas);
		l.setIdCategoria(categoria);
		//l.isBoleanDestacado(destacado);
		l.setEstrellas(estrellas);
		l.setPortadaLibro(portada);
		
		
		
		new LibroDAO().addLibro(l);*/
		
		//request.getRequestDispatcher("Admin.do").forward(request, response); ;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		LibroDAO listaLibros= new LibroDAO();
		listaLibros.deleteAll();
		listaLibros = LectorCsv.readCSV();
		List<Libro> l= listaLibros.getAll();
		
		int ultimoIndice =0;
		
		for(int i = 0; i<l.size();i++) {
			
			ultimoIndice = l.get(i).getIdLibro();
		}
		
		System.out.println(ultimoIndice);
		
		
		PrintWriter out = response.getWriter();
		int id = ultimoIndice+1;
		String nombre = request.getParameter("nombre");
		String autor = request.getParameter("autor");
		String cantParginas = request.getParameter("cantPaginas");
		String categoria= request.getParameter("categoria");
		String destacado =request.getParameter("destacado");
		String estrellas = request.getParameter("estrellas");
		String portada = request.getParameter("portada");
		String idCategoria = request.getParameter("IdCategoria");
		

		String Cadena = id+","+ nombre+","+autor+","+cantParginas+","+destacado+","+portada+","+estrellas+","+idCategoria+","+categoria;
		//System.out.println(Cadena);
		LectorCsv.escribirCSV(Cadena);
	
		out.println(Cadena);
		
		response.sendRedirect("Admin.do");
		
		
	}

}
