package cl.inacap.Ev2CW.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.inacap.Ev2CW.libros.dao.LibroDAO;
import cl.inacap.Ev2CW.libros.dto.LectorCsv;
import cl.inacap.Ev2CW.libros.dto.Libro;

/**
 * Servlet implementation class Detalle
 */
@WebServlet("/Detalle.do")
public class Detalle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Detalle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int indice =  Integer.parseInt(request.getParameter("index"));
		LibroDAO listaLibro = new LibroDAO();
		listaLibro.deleteAll();
		listaLibro = LectorCsv.readCSV();
	
		
		
		request.setAttribute("Libro", listaLibro.getLibroById(indice));
		request.setAttribute("IndexLibro",indice);
		request.getRequestDispatcher("Site/DetalleLibro.jsp").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
