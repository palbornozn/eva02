package cl.inacap.Ev2CW.libros.dto;
import java.io.*;
import java.util.List;

import cl.inacap.Ev2CW.libros.dao.CategoriaDAO;
import cl.inacap.Ev2CW.libros.dao.LibroDAO;


public class LectorCsv {
	

	///// TRAE TODOS LOS REGISTROS DEL CSV //////
	
	public static LibroDAO readCSV() {
		
		
		LibroDAO listaLibro = new LibroDAO();
		
		BufferedReader br = null;
		
	try {
			
			br = new BufferedReader(new FileReader("C:\\Users\\Pato\\eclipse-workspace\\Ev2CW\\Libros.txt"));
			String line = null;
		    while ((line = br.readLine()) != null) {    
		    String[] parts = line.split(",");
		   

		    	Libro l = new Libro();
				l.setIdLibro(Integer.parseInt(parts[0]));
				l.setNombreLibro(parts[1]);
				l.setAutorLibro(parts[2]);
				l.setCantPaginas(Integer.parseInt(parts[3]));
				l.setBoleanDestacado(Boolean.parseBoolean(parts[4]));
				l.setPortadaLibro(parts[5]);
				l.setEstrellas(Integer.parseInt(parts[6]));
				l.setIdCategoria(Integer.parseInt(parts[7]));
				l.setCategoriaLibro(parts[8]);
				listaLibro.addLibro(l);
	
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
	
	return listaLibro;
	}
	
	
	
	
	
	
	
	
	
	
	
	/////  AGREGA UN NUEVO LIBRO AL CSV /////
	
	 public static void escribirCSV(String cadena){
		  File archivo = new File("C:\\Users\\Pato\\eclipse-workspace\\Ev2CW\\Libros.txt");
		   try {
		    boolean append = true;
			FileWriter escribirArchivo = new FileWriter(archivo, append );
		    BufferedWriter buffer = new BufferedWriter(escribirArchivo);
		    buffer.write(cadena);
		    buffer.newLine();
		    buffer.close();
		   }catch (Exception ex) {
		   }
		 }
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 ////// ELIMINA UN REGISTO UNICO DEL CSV //////
	 
	 public static LibroDAO eliminarCSV(int indice) {
		 
		 LibroDAO listaLibro = new LibroDAO();
		 
		 File archivo = new File("C:\\Users\\Pato\\eclipse-workspace\\Ev2CW\\Libros.txt");
	
		 List<Libro> l = listaLibro.getAll();
		 

		 archivo.delete();
		 
	   try {
		   
		  
		   int id = 0;
		    
		   for(int i =0;i < l.size();i++) {
			   
			  
			  
			   if(indice != i) {
			   
			   
			   Libro libro = listaLibro.getLibroById(i);
			   
			 
			   //int id = libro.getIdLibro();
			   String nombre = libro.getNombreLibro();
			   String autor = libro.getAutorLibro();
			   int cant = libro.getCantPaginas();
			   boolean destacado = libro.isBoleanDestacado();
			   String portada = libro.getPortadaLibro();
			   int estrellas = libro.getEstrellas();
			   int categoria = libro.getIdCategoria();
			   String nombreCategoria = libro.getCategoriaLibro();
			  
			   
			   String cadena = id+","+ nombre+","+autor+","+cant+","+destacado+","+portada+","+estrellas+","+categoria+","+nombreCategoria;
			   
			   boolean append = true;	 
			   FileWriter escribirArchivo = new FileWriter(archivo,append);
			   BufferedWriter buffer = new BufferedWriter(escribirArchivo);
			   buffer.write(cadena);
			   buffer.newLine();
		
			   buffer.flush();
			   buffer.close();
			   
			   id = id+1;
			   
			   }else {
				   
				   
				   
				  
			   }
		
		 
		   }
		    
		   
		    
		   }catch (Exception ex) {
		   }
	   
	   			return listaLibro;
		 }
	 
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 ////// LEER CATEGORIAS
	 
	 public static LibroDAO editarCSV(Libro libro) {
		 
		 
		 
		 
		 LibroDAO listaLibro = new LibroDAO();
		 
		 File archivo = new File("C:\\Users\\Pato\\eclipse-workspace\\Ev2CW\\Libros.txt");
	
		 List<Libro> l = listaLibro.getAll();
		 int indice = libro.getIdLibro();

		 archivo.delete();
		 
	   try {
		   
		
		    
		   for(int i =0;i < l.size();i++) {
			   
			  
			  
			   if(i == indice) {		   
			 
			   int id = libro.getIdLibro();
			   String nombre = libro.getNombreLibro();
			   String autor = libro.getAutorLibro();
			   int cant = libro.getCantPaginas();
			   boolean destacado = libro.isBoleanDestacado();
			   String portada = libro.getPortadaLibro();
			   int estrellas = libro.getEstrellas();
			   int categoria = libro.getIdCategoria();
			   String nombreCategoria = libro.getCategoriaLibro();
			  
			   
			   String cadena = id+","+ nombre+","+autor+","+cant+","+destacado+","+portada+","+estrellas+","+categoria+","+nombreCategoria;
			   
			   boolean append = true;	 
			   FileWriter escribirArchivo = new FileWriter(archivo,append);
			   BufferedWriter buffer = new BufferedWriter(escribirArchivo);
			   buffer.write(cadena);
			   buffer.newLine();
		
			   buffer.flush();
			   buffer.close();
			  
			   }else {
				   
				   Libro libroM = listaLibro.getLibroById(i);
				   
					 
				   int id = libroM.getIdLibro();
				   String nombre = libroM.getNombreLibro();
				   String autor = libroM.getAutorLibro();
				   int cant = libroM.getCantPaginas();
				   boolean destacado = libroM.isBoleanDestacado();
				   String portada = libroM.getPortadaLibro();
				   int estrellas = libroM.getEstrellas();
				   int categoria = libroM.getIdCategoria();
				   String nombreCategoria = libroM.getCategoriaLibro();
				  
				   
				   String cadena = id+","+ nombre+","+autor+","+cant+","+destacado+","+portada+","+estrellas+","+categoria+","+nombreCategoria;
				   
				   boolean append = true;	 
				   FileWriter escribirArchivo = new FileWriter(archivo,append);
				   BufferedWriter buffer = new BufferedWriter(escribirArchivo);
				   buffer.write(cadena);
				   buffer.newLine();
			
				   buffer.flush();
				   buffer.close();
				   
				   
				   
				   
				  
			   }
		
		 
		   }
		    
		   
		    
		   }catch (Exception ex) {
		   }
	   
	   			return listaLibro;
		 }

	 
} 
	 
	
	 


