package cl.inacap.Ev2CW.libros.dto;

public class Libro extends CategoriaLibro {
	
	int idLibro;
	String nombreLibro;
	String autorLibro;
	int cantPaginas;
	boolean boleanDestacado;
	String portadaLibro;
	int estrellas;
	boolean isActive;
	

	public void libro(int idLibro, String nombreLibro, String autorLibro, int cantPagina, boolean boleanDestacado,
			String portadaLibro, int estrellas, int idCategoria, boolean isActive) {
		
		this.idLibro = idLibro;
		this.nombreLibro=nombreLibro;
		this.autorLibro=autorLibro;
		this.cantPaginas=cantPagina;
		this.boleanDestacado=boleanDestacado;
		this.portadaLibro=portadaLibro;
		this.estrellas=estrellas;
		this.idCategoria=idCategoria;

		
	
		
	}
	
	public void libro() {
		
	}
	
	
	public int getIdLibro() {
		return idLibro;
	}
	public void setIdLibro(int idLibro) {
		this.idLibro = idLibro;
	}
	public String getNombreLibro() {
		return nombreLibro;
	}
	public void setNombreLibro(String nombreLibro) {
		this.nombreLibro = nombreLibro;
	}
	public String getAutorLibro() {
		return autorLibro;
	}
	public void setAutorLibro(String autorLibro) {
		this.autorLibro = autorLibro;
	}
	public int getCantPaginas() {
		return cantPaginas;
	}
	public void setCantPaginas(int cantPaginas) {
		this.cantPaginas = cantPaginas;
	}
	
	public boolean isBoleanDestacado() {
		return boleanDestacado;
	}
	public void setBoleanDestacado(boolean boleanDestacado) {
		this.boleanDestacado = boleanDestacado;
	}
	
	public String getPortadaLibro() {
		return portadaLibro;
	}
	public void setPortadaLibro(String portadaLibro) {
		this.portadaLibro = portadaLibro;
	}
	
	public int getEstrellas() {
		return estrellas;
	}
	public void setEstrellas(int estrellas) {
		this.estrellas = estrellas;
	}



	

}
