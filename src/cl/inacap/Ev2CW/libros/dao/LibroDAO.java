package cl.inacap.Ev2CW.libros.dao;

import java.util.ArrayList;
import java.util.List;

import cl.inacap.Ev2CW.libros.dto.Libro;

public class LibroDAO {
	
	
	private static List<Libro> arrayLibro = new ArrayList<Libro>();
	
	public  void addLibro(Libro l) {
		arrayLibro.add(l);
	}
	
	public List<Libro> getAll(){
		
		return arrayLibro;
	}
	
	public List<Libro> deleteAll(){
		
		arrayLibro.clear();
		return arrayLibro;
	}
	
	private void eliminarLibro(Libro l, int idLibro) {
		
		arrayLibro.remove(idLibro);
	}
	
	public Libro getLibroById (int idLibro) {
		
		return	arrayLibro.get(idLibro);
	}

	public void delete(Libro l) {
		
		arrayLibro.remove(l);
		
	}
}
